using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerBuilder
    {
        public static Partner CreateBasePartner(bool isActive=true)
        {
            return new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Цветы",
                IsActive = isActive,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
        }

        public static Partner WithLimit(this Partner partner, LimitsCount limitsCount = LimitsCount.OneActive)
        {
            switch (limitsCount)
            {
                case LimitsCount.OneActive:
                    partner.PartnerLimits.Add(new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100,
                        PartnerId = partner.Id,
                        Partner = partner
                    });
                    break;
                case LimitsCount.OneNotActive:
                    partner.PartnerLimits.Add(new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("c9bef066-3c5a-4e5d-9cff-bd54479f075e"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        CancelDate = new DateTime(2020, 8, 9),
                        Limit = 100,
                        PartnerId = partner.Id,
                        Partner = partner
                    });
                    break;
            }
            return partner;
        }

        public enum LimitsCount
        {
            None,
            OneActive,
            OneNotActive
        }
    }
}