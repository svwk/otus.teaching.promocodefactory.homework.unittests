using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Services;
using Otus.Teaching.PromoCodeFactory.Core.Services;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Services.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly Mock<IDateTimeService> _dateTimeServiceMock;
        private readonly IPartnerService _partnerService;
        private SetPartnerPromoCodeLimitRequest request;
        private Partner partner;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _dateTimeServiceMock = fixture.Freeze<Mock<IDateTimeService>>();
            _partnerService = fixture.Build<PartnerService>().Create();
            request = fixture.Create<SetPartnerPromoCodeLimitRequest>();
            partner = PartnerBuilder.CreateBasePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
        }
        
        [Theory, AutoData]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ThrowsEntityNotFoundException(Guid partnerId)
        {
            // Arrange
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync((Partner) null);

            // Act
            Func<Task<Guid>> act = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partnerId,request);

            // Assert
            await act.Should().ThrowAsync<EntityNotFoundException>("Данный партнер не активен");
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ThrowsChangePartnerLimitException()
        {
            // Arrange
            partner.IsActive = false;
            
            // Act
            Func<Task> act = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id,request);
        
            // Assert
            await act.Should().ThrowAsync<ChangePartnerLimitException>("Лимит должен быть больше 0");
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitWithOldLimitActual_NumberIssuedPromoCodesSetTo0()
        {
            // Arrange
            partner.WithLimit();
            // Act
           await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id,request);
            // Assert
            partner.NumberIssuedPromoCodes.Equals(0);
        }
        
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitWithOldLimitNotActual_NumberIssuedPromoCodesNotChanged()
        {
            // Arrange
            partner.WithLimit(PartnerBuilder.LimitsCount.OneNotActive);
          
            int oldNumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;
          

            // Act
            await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id,request);
            // Assert
            partner.NumberIssuedPromoCodes.Equals(oldNumberIssuedPromoCodes);
            
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitIsSetting_OldLimitDeactivate()
        {
            // Arrange
            partner.WithLimit();
            DateTime dayNow = DateTime.Now;
            _dateTimeServiceMock.Setup(x => x.CurrentDateTime()).
                Returns(dayNow);

            // Act
            await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id,request);
            
            // Assert
            partner.PartnerLimits.FirstOrDefault(x => (x.CancelDate == dayNow) & (x.Id != default(Guid))).Should()
                .NotBeNull();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitIsSetting_NewLimitGreater0()
        {
            // Arrange
            request.Limit = 0;

            // Act
            Func<Task> act = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id,request);
            // Assert
            await act.Should().ThrowAsync<ChangePartnerLimitException>("Лимит должен быть больше 0");
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitIsSetting_NewLimitIsSavedToDB()
        {
            // Arrange

            // Act
            await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id,request);
            
            // Assert
            _partnersRepositoryMock.Verify(repo => repo.UpdateAsync(It.IsAny<Partner>()));
        }

    }
}