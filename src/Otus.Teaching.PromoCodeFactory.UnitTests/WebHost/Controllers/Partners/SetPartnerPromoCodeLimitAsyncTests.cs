﻿using System;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Services;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IPartnerService> _partnersServiceMock;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersServiceMock = fixture.Freeze<Mock<IPartnerService>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Theory, AutoData]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsAny_PartnerServiceIsCalled(Guid partnerId, SetPartnerPromoCodeLimitRequest request)
        {
            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            _partnersServiceMock.Verify(x => x.SetPartnerPromoCodeLimitAsync(partnerId, request));
        }


        [Theory, AutoData]
        public async void SetPartnerPromoCodeLimitAsync_EntityNotFoundException_ShouldReturnNotFound(Guid partnerId, SetPartnerPromoCodeLimitRequest request)
        {
            // Arrange
            _partnersServiceMock.Setup(x =>
                    x.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), It.IsAny<SetPartnerPromoCodeLimitRequest>()))
                .Throws<EntityNotFoundException>();
           
            // Act
            var result = await  _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }


        [Theory, AutoData]
        public async void SetPartnerPromoCodeLimitAsync_ChangePartnerLimitException_ShouldReturnBadRequest(Guid partnerId, SetPartnerPromoCodeLimitRequest request)
        {
            // Arrange
            _partnersServiceMock.Setup(x =>
                    x.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), It.IsAny<SetPartnerPromoCodeLimitRequest>()))
                .Throws<ChangePartnerLimitException>();
           
            // Act
            var result = await  _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestResult>();
        }
    }
}