﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Interfaces.Services;

namespace Otus.Teaching.PromoCodeFactory.Core.Services
{
    public class PartnerService
        : IPartnerService
    {
        private readonly IRepository<Partner> _partnersRepository;
        private readonly IDateTimeService _dateTimeService;

        public PartnerService(IRepository<Partner> partnersRepository,  
            IDateTimeService currentDateTimeService)
        {
            _partnersRepository = partnersRepository;
            _dateTimeService = currentDateTimeService;
        }
        
        public async Task<List<PartnerResponse>> GetPartnersAsync()
        {
            var partners = await _partnersRepository.GetAllAsync();

            var response = partners.Select(x => new PartnerResponse()
            {
                Id = x.Id,
                Name = x.Name,
                NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = x.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponse()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = _dateTimeService.DataFormat(y.CreateDate),
                        EndDate = _dateTimeService.DataFormat(y.EndDate),
                        CancelDate = _dateTimeService.DataFormat(y.CancelDate),
                    }).ToList()
            }).ToList();

            return response;
        }

        public async Task<PartnerResponse> GetPartnerByIdAsync(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);
            if (partner == null)
                throw new EntityNotFoundException();
            var response = new PartnerResponse()
            {
                Id = partner.Id,
                Name = partner.Name,
                NumberIssuedPromoCodes = partner.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = partner.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponse()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = _dateTimeService.DataFormat(y.CreateDate),
                        EndDate = _dateTimeService.DataFormat(y.EndDate),
                        CancelDate = _dateTimeService.DataFormat(y.CancelDate),
                    }).ToList()
            };

            return response;
        }

        public async Task<PartnerPromoCodeLimitResponse> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if (partner == null)
                throw new EntityNotFoundException();
            
            var limit = partner.PartnerLimits
                .FirstOrDefault(x => x.Id == limitId);

            var response = new PartnerPromoCodeLimitResponse()
            {
                Id = limit.Id,
                PartnerId = limit.PartnerId,
                Limit = limit.Limit,
                CreateDate = _dateTimeService.DataFormat(limit.CreateDate),
                EndDate = _dateTimeService.DataFormat(limit.EndDate),
                CancelDate = _dateTimeService.DataFormat(limit.CancelDate),
            };

            return response;
        }

        public async Task<Guid> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);

            if (partner == null)
                throw new EntityNotFoundException();
            
            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new ChangePartnerLimitException("Данный партнер не активен");
            
            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;
                
                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate =_dateTimeService.CurrentDateTime();
            }

            if (request.Limit <= 0)
                throw new ChangePartnerLimitException("Лимит должен быть больше 0");
            
            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = request.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = _dateTimeService.CurrentDateTime(),
                EndDate = request.EndDate
            };
            
            partner.PartnerLimits.Add(newLimit);

            await _partnersRepository.UpdateAsync(partner);
            
            return newLimit.Id;
        }

        public async Task CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            var partner = await _partnersRepository.GetByIdAsync(id);
            
            if (partner == null)
                throw new EntityNotFoundException();
            
            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new ChangePartnerLimitException("Данный партнер не активен");
            
            //Отключение лимита
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => 
                !x.CancelDate.HasValue);
            
            if (activeLimit != null)
            {
                activeLimit.CancelDate = _dateTimeService.CurrentDateTime();
            }

            await _partnersRepository.UpdateAsync(partner);
        }
    }
}