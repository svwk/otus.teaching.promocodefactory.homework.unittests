﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Dto
{
    public class SetPartnerPromoCodeLimitRequest
    {
        public DateTime EndDate { get; set; }
        public int Limit { get; set; }
    }
}